import os
from dotenv import load_dotenv
load_dotenv()

API_SECRET_KEY = os.getenv("API_SECRET_KEY")
API_SALT = os.getenv("API_SALT")