from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from django import views
from rest_framework import views, status
from rest_framework.response import Response

from authentication.authentication import ExternalAuthentication
from movies.services import MovieService

class Movie(views.APIView):
    authentication_classes = (ExternalAuthentication, )

    @method_decorator(cache_page(60))
    def get(self, *args, pk):
        try:
            service_instance = MovieService(pk)
            data = service_instance.fetch_movie_data()
            return Response({ "status": "SUCCESS", "data": data })
        except Exception as e:
            return Response({ "status": "FAILURE", "error": str(e) }, status=status.HTTP_400_BAD_REQUEST)