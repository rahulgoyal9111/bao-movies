from django.test import TestCase
import requests

from authentication.services import generate_auth_token

class MovieAPITestCase(TestCase):

    # This test will fail if token is valid
    def test_movie_fetch_missing_auth(self):
        request = requests.get("http://localhost:8000/movies/d6bd6efc-37b2-4c40-b092-367cea8c88fe/")
        self.assertEqual(403, request.status_code, "Permission Denied")

    # This test will fail if ID provided is not correct
    def test_movie_fetch_data(self):
        token = generate_auth_token()
        request = requests.get("http://localhost:8000/movies/d6bd6efc-37b2-4c40-b092-367cea8c88fe/", \
            headers={ "ghiblikey": token })
        self.assertEqual(200, request.status_code, "Data fetch successfully")

    # This test will fail if ID provided is correct
    def test_movie_fetch_data_invalid_id(self):
        token = generate_auth_token()
        request = requests.get("http://localhost:8000/movies/d6bd6efc-37b2-4c40-b092-367cea8c88f0/", \
            headers={ "ghiblikey": token })
        self.assertNotEqual(200, request.status_code, "Invalid Data Failure")