import requests
import json

class MovieService:
    def __init__(self, movie_id) -> None:
        self.movie_id = movie_id
        self.urls = {
            "movie_by_id": "https://ghibli.rest/films?id={}"
        }

    def fetch_movie(self):
        try:
            result = requests.get(url=self.urls["movie_by_id"].format(self.movie_id))
            if result.status_code == 200:
                data = json.loads(result.content.decode("utf-8"))
                if data is not None and len(data) > 0:
                    object = data[0]
                    return object
                else:
                    raise Exception("Some error occurred while fetching data")
            else:
                raise Exception("Some error occurred while fetching data")
        except Exception as e:
            raise e

    def fetch_actors(self, list = []):
        if list is None or len(list) == 0:
            return []

        try:
            response = []
            for actor in list:
                result = requests.get(url=actor)
                if result.status_code == 200:
                    data = json.loads(result.content.decode("utf-8"))
                    if data is not None and len(data) > 0:
                        object = data[0]
                        del object["gender"]
                        del object["age"]
                        del object["eye_color"]
                        del object["hair_color"]
                        del object["films"]
                        response.append(object)
                    else:
                        raise Exception("Some error occurred while fetching actor data")
                else:
                    raise Exception("Some error occurred while fetching actor data")

            return response
        except Exception as e:
            raise e

    def fetch_species(self, url):
        if url is None:
            return []

        try:
            result = requests.get(url=url)
            if result.status_code == 200:
                data = json.loads(result.content.decode("utf-8"))
                if data is not None and len(data) > 0:
                    object = data[0]
                    del object["people"]
                    del object["films"]
                    return object
                else:
                    raise Exception("Some error occurred while fetching species data")
            else:
                raise Exception("Some error occurred while fetching species data")

        except Exception as e:
            raise e


    def fetch_movie_data(self):
        movie_object = self.fetch_movie()
        actors = self.fetch_actors(movie_object.get("people"))

        cache_object = {}
        for index, actor in enumerate(actors):
            specie = actor.get("species")
            if cache_object.get(specie):
                actors[index]["species"] = cache_object.get(specie)
            else:
                species = self.fetch_species(specie)
                actors[index]["species"] = species
                cache_object.update({specie: species})

        del movie_object["people"]
        movie_object["actors"] = actors

        return movie_object