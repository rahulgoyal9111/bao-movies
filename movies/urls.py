from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from movies.views import Movie

urlpatterns = [
    path('<uuid:pk>/', Movie.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)