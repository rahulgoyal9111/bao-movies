import hmac, hashlib
from datetime import datetime

from bao.config import API_SECRET_KEY, API_SALT

def generate_auth_token(token_validity: str = 'day') -> str or None:
    """Method to generate token for notifications apis.
    Args:
        token_validity (str): Period for which token will be valid.
    Returns:
        (str): Generated token or None if an exception arises.
    """
    try:
        #generates token with validity of day, month or year
        if token_validity == 'day':
            token_validity = datetime.now().strftime('%d%m%Y')
        elif token_validity == 'month':
            token_validity = datetime.now().strftime('%m%Y')
        elif token_validity == 'year':
            token_validity = datetime.now().strftime('%Y')
        else:
            return None
        secretKey = "{}{}".format(API_SECRET_KEY, token_validity)
        updateKey = API_SALT.encode('utf-8')
        dataToSign = secretKey.encode('utf-8')
        return hmac.new(updateKey, dataToSign, digestmod = hashlib.sha256).hexdigest()
    except Exception as ex:
        return None