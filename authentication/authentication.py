from rest_framework import exceptions, authentication
from authentication.services import generate_auth_token

# Generate Token
"""
generatedToken = generate_auth_token()
print(generatedToken)
"""

"""
External authentication - token based
"""
class ExternalAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        try:
            token = request.headers['ghiblikey']
            generatedToken = generate_auth_token()
        except IndexError:
            raise exceptions.AuthenticationFailed('[Index Error]: Invalid API Key')
        except Exception as e:
            raise exceptions.AuthenticationFailed('Invalid API Key')

        if token != generatedToken:
            raise exceptions.AuthenticationFailed('API Key not matched')
        else:
            return None

