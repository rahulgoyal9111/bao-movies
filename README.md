# Project Setup
- Commands to run:
    Setup env: ```python3 -m venv env```
    Activate env: ```. env/bin/activate```
    Install all requirements: ```python3 -m pip install -r requirements.txt```
    Runserver: ```python3 manage.py runserver```

- Code will be up and running on port 8000 by default

# Project Information
- API to be consumed:
    API Key: 
        - In headers with name: 'ghiblikey'
        - How to generate: 
            - API Key can be generated from shell, referring to file `authentication.services.py`
            - Otherwise, uncomment line 5-8 in `authentication/authentication.py` and a token will be rendered in the terminal that can utilised for authentication.
    URL (GET METHOD): {{URL}}/movies/<uuid>/

- Page caching has been implemented with time of 60s to make sure data retrieved from the API is no less than 1 minute old and if multiple API hits are received within a minute then data will be served from cache.
    - We can make it more persistent using redis or CDN

- To run tests execute command:
    ```python3 manage.py test```